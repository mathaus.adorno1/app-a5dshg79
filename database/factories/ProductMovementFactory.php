<?php

namespace Database\Factories;

use App\Models\ProductMovement;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<ProductMovement>
 */
class ProductMovementFactory extends Factory
{
    protected $model = ProductMovement::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
