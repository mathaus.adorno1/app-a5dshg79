<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->group(function () {
    Route::resource('products', 'App\Http\Controllers\ProductController')
        ->only(['index', 'store', 'show', 'update', 'destroy']);

    Route::resource('product-movement', 'App\Http\Controllers\ProductMovementController')
        ->only(['index', 'store', 'show', 'update', 'destroy']);
});

Route::post('login', 'App\Http\Controllers\AuthController@login');
