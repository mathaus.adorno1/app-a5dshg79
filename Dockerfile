FROM php:8.1-fpm-alpine3.14

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

RUN apk add --no-cache \
    openssl \
    ca-certificates \
    libxml2-dev \
    oniguruma-dev \
    libpng-dev \
    libjpeg-turbo \
    libjpeg-turbo-dev

RUN install-php-extensions \
    bcmath \
    gd \
    ctype \
    dom \
    fileinfo \
    mbstring \
    pdo pdo_mysql \
    tokenizer \
    pcntl \
    xsl \
    exif \
    soap \
    zip \
    mongodb-stable


WORKDIR /var/www/html

RUN chown www-data:www-data /var/www/html

COPY --chown=www-data:www-data . .

COPY --from=composer /usr/bin/composer /usr/bin/composer

USER www-data

RUN composer install --no-dev --prefer-dist

USER root

EXPOSE 9000
