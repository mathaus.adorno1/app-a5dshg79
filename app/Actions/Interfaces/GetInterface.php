<?php

namespace App\Actions\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface GetInterface
{
    /**
     * @param  array  $data
     * @return Model
     */
    public function execute(array $data): Model;
}
