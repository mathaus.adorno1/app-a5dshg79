<?php

namespace App\Actions\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface UpdateInterface
{
    /**
     * @param  array  $data
     * @param  int  $id
     * @return Model
     */
    public function execute(array $data, int $id): Model;
}
