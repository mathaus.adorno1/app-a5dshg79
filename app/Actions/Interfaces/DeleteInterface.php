<?php

namespace App\Actions\Interfaces;

interface DeleteInterface
{
    public function execute(int $id): bool;
}
