<?php

namespace App\Actions\Interfaces;

use Illuminate\Database\Eloquent\Collection;

interface ListInterface
{
    /**
     * @param  array  $data
     * @return Collection
     */
    public function execute(array $data): Collection;
}
