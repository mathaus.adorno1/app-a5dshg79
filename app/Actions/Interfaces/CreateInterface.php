<?php

namespace App\Actions\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface CreateInterface
{
    /**
     * @param  array  $data
     * @return Model
     */
    public function execute(array $data): Model;
}
