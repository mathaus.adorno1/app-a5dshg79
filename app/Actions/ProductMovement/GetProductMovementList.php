<?php

namespace App\Actions\ProductMovement;

use App\Actions\Interfaces\ListInterface;
use App\Actions\Traits\Get;
use App\Models\ProductMovement;
use App\Repositories\ProductRepository;
use Illuminate\Database\Eloquent\Collection;

class GetProductMovementList implements ListInterface
{
    use Get;

    protected string $model = ProductMovement::class;

    public function __construct(
        private ProductRepository $productRepository
    ) {
    }

    public function execute(array $data): Collection
    {
        return $this->productRepository->listByFilters($data);
    }
}
