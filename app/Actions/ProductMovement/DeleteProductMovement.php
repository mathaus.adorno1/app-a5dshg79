<?php

namespace App\Actions\ProductMovement;

use App\Actions\Interfaces\DeleteInterface;
use App\Actions\Product\UpdateProduct;
use App\Actions\Traits\Delete;
use App\Actions\Traits\Get;
use App\Models\ProductMovement;

class DeleteProductMovement implements DeleteInterface
{
    use Delete, Get;

    protected string $model = ProductMovement::class;

    public function __construct(
        private UpdateProduct $updateProduct
    ) {
    }

    public function execute(int $id): bool
    {
        $productMovement = $this->get($id);

        $quantity = $productMovement['type'] == 'in' ?
            $productMovement['quantity'] * -1 :
            $productMovement['quantity'];

        $product = $productMovement->product;

        $updateProductData = [
            'quantity' => $product->quantity + $quantity,
        ];

        $this->updateProduct->execute($updateProductData, $product->id);

        return $this->delete($id);
    }
}
