<?php

namespace App\Actions\ProductMovement;

use App\Actions\Interfaces\CreateInterface;
use App\Actions\Product\UpdateProduct;
use App\Actions\Traits\Create;
use App\Models\ProductMovement;
use App\Repositories\ProductRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CreateProductMovement implements CreateInterface
{
    use Create;

    protected string $model = ProductMovement::class;

    public function __construct(
        private ProductRepository $productRepository,
        private UpdateProduct $updateProduct
    ) {
    }

    /**
     * @param  array  $data
     * @return Model
     */
    public function execute(array $data): Model
    {
        return DB::transaction(function () use ($data) {
            $product = $this->productRepository->findByFilters($data);

            $quantity = $data['type'] == 'in' ? $data['quantity'] : $data['quantity'] * -1;

            $updateProductData = [
                'quantity' => $product->quantity + $quantity,
            ];

            $product = $this->updateProduct->execute($updateProductData, $product->id);

            $data['relations'] = [
                'product' => $product,
            ];

            return $this->create($data);
        });
    }
}
