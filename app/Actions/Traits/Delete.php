<?php

namespace App\Actions\Traits;

trait Delete
{
    public function delete(int $id): bool
    {
        $model = new $this->model;
        $model = $model->findOrFail($id);
        $model->delete();

        return true;
    }
}
