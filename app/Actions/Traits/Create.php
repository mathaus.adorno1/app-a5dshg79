<?php

namespace App\Actions\Traits;

use Illuminate\Database\Eloquent\Model;

trait Create
{
    public function create(array $data): Model
    {
        $model = new $this->model;

        if (! empty($data['relations'])) {
            foreach ($data['relations'] as $relation => $value) {
                $model->$relation()->associate($value);
            }
        }

        $fields = $model->getFillable();
        $data = collect($data)->only($fields)->toArray();
        $model->fill($data);
        $model->save();

        return $model;
    }
}
