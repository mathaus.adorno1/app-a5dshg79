<?php

namespace App\Actions\Traits;

use Illuminate\Database\Eloquent\Model;

trait Update
{
    public function update(int $id, array $data): Model
    {
        $model = new $this->model;
        $model = $model->findOrFail($id);
        $model->fill($data);
        $model->save();

        return $model;
    }
}
