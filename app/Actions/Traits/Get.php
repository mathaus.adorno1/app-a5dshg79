<?php

namespace App\Actions\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

trait Get
{
    public function get(int $id): Model
    {
        $model = new $this->model;

        return $model->findOrFail($id);
    }

    public function list(array $data): Collection|LengthAwarePaginator
    {
        $model = new $this->model;

        return $model->where($data)->paginate();
    }
}
