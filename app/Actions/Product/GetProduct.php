<?php

namespace App\Actions\Product;

use App\Actions\Interfaces\GetInterface;
use App\Actions\Traits\Get;
use App\Models\Product;
use App\Repositories\ProductRepository;
use Illuminate\Database\Eloquent\Model;

class GetProduct implements GetInterface
{
    use Get;

    public function __construct(
        private ProductRepository $productRepository
    ) {
    }

    protected string $model = Product::class;

    public function execute(array $data): Model
    {
        return $this->productRepository->findByFilters($data);
    }
}
