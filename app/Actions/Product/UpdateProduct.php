<?php

namespace App\Actions\Product;

use App\Actions\Interfaces\UpdateInterface;
use App\Actions\Traits\Update;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UpdateProduct implements UpdateInterface
{
    use Update;

    protected string $model = Product::class;

    public function execute(array $data, int $id): Model
    {
        return DB::transaction(fn () => $this->update($id, $data));
    }
}
