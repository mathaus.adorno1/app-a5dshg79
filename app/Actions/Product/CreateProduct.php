<?php

namespace App\Actions\Product;

use App\Actions\Interfaces\CreateInterface;
use App\Actions\Traits\Create;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CreateProduct implements CreateInterface
{
    use Create;

    protected string $model = Product::class;

    /**
     * {@inheritDoc}
     */
    public function execute(array $data): Model
    {
        return DB::transaction(fn () => $this->create($data));
    }
}
