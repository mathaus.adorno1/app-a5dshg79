<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductMovement extends Model
{
    protected $fillable = [
        'quantity',
        'type',
    ];

    use HasFactory;

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
