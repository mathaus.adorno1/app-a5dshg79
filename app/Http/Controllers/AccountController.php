<?php

namespace App\Http\Controllers;

use App\Services\CreateAccountService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AccountController extends Controller
{
    /**
     * @param  CreateAccountService  $createAccountService
     */
    public function __construct(
        private CreateAccountService $createAccountService
    ) {
    }

    /**
     * @param  Request  $request
     * @return array
     *
     * @throws ValidationException
     */
    public function store(Request $request): array
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'taxvat' => 'required|string|min:11|max:14',
            'birth_date' => 'required|date',
            'password' => 'required|string|min:8',
        ]);

        if ($validator->fails()) {
            return ['errors' => $validator->errors()->toArray()];
        }

        return $this->createAccountService->create($validator->validate());
    }
}
