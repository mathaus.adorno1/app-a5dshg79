<?php

namespace App\Http\Controllers;

use App\Actions\ProductMovement\CreateProductMovement;
use App\Actions\ProductMovement\DeleteProductMovement;
use App\Actions\ProductMovement\GetProductMovementList;
use App\Http\Resources\ProductMovementResource;
use App\Models\ProductMovement;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductMovementController extends Controller
{
    public function __construct(
        private CreateProductMovement $createProductMovement,
        private DeleteProductMovement $deleteProductMovement,
        private GetProductMovementList $getProductMovementList
    ) {
    }

    /**
     * @param  Request  $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $validated = $request->validate([
            'start_date' => 'nullable|date',
            'end_date' => 'nullable|date|after:start_date',
        ]);

        return ProductMovementResource::collection($this->getProductMovementList->execute($validated));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return ProductMovementResource
     */
    public function store(Request $request): ProductMovementResource
    {
        $validated = $request->validate([
            'sku' => 'required|numeric|exists:products,sku',
            'quantity' => 'required|numeric',
            'type' => 'required|string|in:in,out',
        ]);

        $productMovement = $this->createProductMovement->execute($validated);

        return new ProductMovementResource($productMovement);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ProductMovement  $productMovement
     * @return JsonResponse
     */
    public function destroy(ProductMovement $productMovement)
    {
        $this->deleteProductMovement->execute($productMovement->id);

        return response()->json(null, 204);
    }
}
