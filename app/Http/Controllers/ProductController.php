<?php

namespace App\Http\Controllers;

use App\Actions\Product\CreateProduct;
use App\Actions\Product\GetProduct;
use App\Actions\Product\UpdateProduct;
use App\Actions\ProductMovement\GetProductMovementList;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductController extends Controller
{
    public function __construct(
        private CreateProduct $createProduct,
        private UpdateProduct $updateProduct,
        private GetProduct $getProduct,
        private GetProductMovementList $getProductMovementList
    ) {
    }

    /**
     * @param  Request  $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $validated = $request->validate([
            'start_date' => 'string|nullable|date',
            'end_date' => 'string|nullable|date|after:start_date',
        ]);

        return ProductResource::collection($this->getProductMovementList->execute($validated));
    }

    public function store(Request $request): ProductResource
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'sku' => 'required|numeric',
            'quantity' => 'required|numeric',
        ]);

        $product = $this->createProduct->execute($validated);

        return new ProductResource($product);
    }

    public function show($id): ProductResource
    {
        $product = $this->getProduct->execute(['id' => $id]);

        return new ProductResource($product);
    }

    public function update(Request $request, $id): ProductResource
    {
        $validated = $request->validate([
            'sku' => 'required|numeric',
            'quantity' => 'required|numeric',
            'type' => 'required|string|in:in,out',
        ]);

        $product = $this->updateProduct->execute($validated, $id);

        return new ProductResource($product);
    }
}
