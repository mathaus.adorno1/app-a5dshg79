<?php

namespace App\Http\Controllers;

use App\Services\CreateOAuthToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * @param  CreateOAuthToken  $createOAuthToken
     */
    public function __construct(
        private CreateOAuthToken $createOAuthToken
    ) {
    }

    /**
     * @throws ValidationException
     */
    public function login(Request $request): array
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return ['errors' => $validator->errors()->toArray()];
        }

        $validated = $validator->validate();

        return $this->createOAuthToken->create($validated['email'], $validated['password']);
    }
}
