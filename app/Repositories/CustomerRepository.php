<?php

namespace App\Repositories;

use App\Providers\Interfaces\CustomerRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class CustomerRepository implements CustomerRepositoryInterface
{
//    protected $model = Custo
    public function __construct()
    {
    }

    public function find(int $id): Model
    {
        // TODO: Implement find() method.
    }

    public function list(array $filters, int $page, int $perPage): LengthAwarePaginator
    {
        // TODO: Implement list() method.
    }

    public function query(): Builder
    {
        // TODO: Implement query() method.
    }
}
