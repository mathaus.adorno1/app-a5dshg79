<?php

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class ProductRepository
{
    protected string $model = Product::class;

    public function findByFilters(array $data): ?Model
    {
        $query = Product::query();
        $query = $this->buildFilters($data, $query);

        return $query->first();
    }

    public function listByFilters(array $data): ?Collection
    {
        $query = Product::query();
        $query = $this->buildFilters($data, $query);

        return $query->get();
    }

    private function buildFilters(array $data, Builder $query): Builder
    {
        $data = collect($data);

        $query->when($data->get('sku'), function ($query, $sku) {
            $query->where('sku', 'like', "%{$sku}%");
        });

        $query->when($data->get('start_date'), function ($query, $start_date) {
            $query->where('created_at', '>=', $start_date);
        });

        $query->when($data->get('end_date'), function ($query, $end_date) {
            $query->where('created_at', '<=', $end_date);
        });

        return $query;
    }
}
