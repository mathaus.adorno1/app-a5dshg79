<?php

namespace App\GraphQL\Mutations\Products;

use App\Actions\Product\CreateProduct as CreateProductAction;
use GraphQL\Type\Definition\Type as GraphQLType;
use Illuminate\Database\Eloquent\Model;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class CreateProduct extends Mutation
{
    protected $attributes = [
        "name" => "createProduct",
        "descriptions" => "Create a product"
    ];
    /**
     * @param  CreateProductAction  $createProduct
     */
    public function __construct(
        private CreateProductAction $createProduct
    ) {
    }

    public function args(): array
    {
        return [
            'sku' => [
                'name' => 'sku',
                'type' => GraphQLType::string(),
                'rules' => ['required'],
            ],
            'quantity' => [
                'name' => 'quantity',
                'type' => GraphQLType::int(),
                'rules' => ['required'],
            ],
            'name' => [
                'name' => 'name',
                'type' => GraphQLType::string(),
                'rules' => ['required'],
            ],
        ];
    }

    public function type(): GraphQLType
    {
        return GraphQL::type('Product');
    }

    public function resolve($root, array $args): Model
    {
        return $this->createProduct->execute($args);
    }
}
