<?php

namespace App\GraphQL\Types;

use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserType extends GraphQLType
{
    /**
     * @var string[]
     */
    protected $attributes = [
        'name' => 'User',
        'description' => 'Collection of users and details',
        'model' => User::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Id of particular user',
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Name of particular user',
            ],
            'email' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Email of particular user',
            ],
        ];
    }
}
