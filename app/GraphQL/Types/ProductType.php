<?php

namespace App\GraphQL\Types;

use App\Models\Product;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Product',
        'description' => 'Collection of customers and details',
        'model' => Product::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Id of particular product',
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Name of particular product',
            ],

            'sku' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Description of particular product',
            ],
            'quantity' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Image of particular product',
            ],
            'created_at' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Created at of particular product',
            ],
            'updated_at' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Updated at of particular product',
            ],
        ];
    }
}
