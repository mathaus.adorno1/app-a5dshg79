<?php

namespace App\GraphQL\Types;

use App\Models\Customer;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CustomerType extends GraphQLType
{
    /**
     * @var string[]
     */
    protected $attributes = [
        'name' => 'Customer',
        'description' => 'Collection of customers and details',
        'model' => Customer::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Id of particular customer',
            ],
            'first_name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'First name of particular customer',
            ],
            'last_name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Last name of particular customer',
            ],
            'taxvat' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Taxvat of particular customer',
            ],
            'email' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Email of particular customer',
            ],
            'birth_date' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Birth date of particular customer',
            ],
            'user' => [
                'type' => GraphQL::type('User'),
                'description' => 'User from customer',
            ],
        ];
    }
}
