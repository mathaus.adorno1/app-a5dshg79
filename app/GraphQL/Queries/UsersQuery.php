<?php

namespace App\GraphQL\Queries;

use App\Models\User;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\Type as GraphQLType;
use Illuminate\Database\Eloquent\Collection;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class UsersQuery extends Query
{
    public function type(): GraphQLType
    {
        return Type::listOf(GraphQL::type('User'));
    }

    public function resolve($root, $args): Collection
    {
        return User::all();
    }
}
