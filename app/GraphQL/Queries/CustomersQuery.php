<?php

namespace App\GraphQL\Queries;

use App\Models\Customer;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\Type as GraphQLType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class CustomersQuery extends Query
{
    protected $attributes = [
        'name' => 'customers',
    ];

    public function type(): GraphQLType
    {
        return Type::listOf(GraphQL::type('Customer'));
    }

    public function resolve($root, array $args): Collection
    {
        if(Cache::has('products')) {
           return Cache::get('products');
        }

        $customers = Customer::all();
        Cache::put('products', $customers);
        return $customers;
    }
}
