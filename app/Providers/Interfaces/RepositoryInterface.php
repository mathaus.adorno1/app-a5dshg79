<?php

namespace App\Providers\Interfaces;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

interface RepositoryInterface
{
    public function find(int $id): Model;

    public function list(array $filters, int $page, int $perPage): LengthAwarePaginator;

    public function query(): Builder;
}
