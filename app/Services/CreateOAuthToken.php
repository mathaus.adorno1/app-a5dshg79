<?php

namespace App\Services;

use Laravel\Passport\Client;
use Laravel\Passport\ClientRepository;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Laravel\Passport\Passport;
use Psr\Http\Message\ServerRequestInterface;

class CreateOAuthToken
{
    /**
     * @param  ServerRequestInterface  $server
     * @param  AccessTokenController  $accessTokenController
     * @param  ClientRepository  $clientRepository
     */
    public function __construct(
        private ServerRequestInterface $server,
        private AccessTokenController $accessTokenController,
        private ClientRepository $clientRepository,
    ) {
    }

    /**
     * @param  string  $email
     * @param  string  $password
     * @return array
     */
    public function create(string $email, string $password): array
    {
        $client = $this->getClient();

        $token = $this->accessTokenController->issueToken($this->server->withParsedBody(
            [
                'grant_type' => 'password',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'scope' => '',
                'username' => $email,
                'password' => $password,
            ]
        ));

        return json_decode($token->content(), true);
    }

    private function getClient(): Client
    {
        $client = Passport::client()->newQuery()
            ->where('password_client', true)
            ->where('name', 'Laravel')->first();

        if (! $client) {
            $client = $this->clientRepository->createPasswordGrantClient(
                null,
                'Laravel',
                'http://localhost',
                'users'
            );
        }

        return $client;
    }
}
