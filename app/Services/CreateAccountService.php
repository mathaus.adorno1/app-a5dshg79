<?php

namespace App\Services;

use App\Models\Customer;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreateAccountService
{
    /**
     * @param  CreateOAuthToken  $authToken
     */
    public function __construct(
        private CreateOAuthToken $authToken
    ) {
    }

    /**
     * @param  array  $params
     * @return array
     */
    public function create(array $params): array
    {
        $collection = collect($params);
        $user = new User;
        $customer = new Customer;
        DB::beginTransaction();
        $user = $this->storeUser($user, $collection->only($user->getFillable())->toArray());
        $customer = $this->storeCustomer($customer, $user, $collection->only($customer->getFillable())->toArray());
        $token = $this->authToken->create($params['email'], $params['password']);
        DB::commit();

        return [
            'customer' => $customer,
            'access_token' => $token,
        ];
    }

    private function storeUser(User $user, array $data): User
    {
        $user->fill($data);
        $user->password = Hash::make($user->password);
        $user->save();

        return $user;
    }

    /**
     * @param  Customer  $customer
     * @param  User  $user
     * @param  array  $data
     * @return Customer
     */
    private function storeCustomer(Customer $customer, User $user, array $data): Customer
    {
        $customer->fill($data);
        $customer->user()->associate($user);
        $customer->save();

        return $customer;
    }
}
