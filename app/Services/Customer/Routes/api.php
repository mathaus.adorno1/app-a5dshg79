<?php

use App\Http\Controllers\AccountController;
use Illuminate\Support\Facades\Route;

Route::prefix('api')
    ->resource('customers', AccountController::class)
    ->only('store');
