# Teste técnico Appmax

##Tecnologias utilizadas
- PHP: 8.1
- Laravel Framework: 9.41
- Mysql: 8.0
- Nginx
- [Pint CS Fixer] (https://laravel.com/docs/9.x/pint)
- Docker

## Instalação

- Clone o repositório
- Crie um arquivo .env na raiz do projeto e copie o conteúdo do arquivo .env.example
- Rodar o comando "docker-compose up -d" na raiz do projeto
- Entrar no container da aplicação com o comando "docker exec -it laravel_php sh"
- Rodar o comando "chmod 777 -R storage" para dar permissão de escrita na pasta storage do projeto (permissões do container)
- Rodar o comando "composer install"
- Rodar o comando "php artisan key:generate"
- Rodar o comando "php artisan migrate"
- Rodar o comando "php artisan test" para rodar os testes automatizados
- Rodar o comando "php artisan db:seed" para rodar os seeders e popular o banco de dados
## Rotas
- Autenticação: http://localhost:8080/api/auth/login

### Produtos
- GET /api/products
    - Retorna todos os produtos cadastrados
- GET /api/products/{id}
    - Retorna o produto com o id informado
- POST /api/products
    - Cadastra um novo produto 
- PUT /api/products/{id}
### Movimentação de produtos
- POST /api/product-movement
    - Cadastra um novo movimento de produto e atualiza o estoque
- GET /api/product-movement
    - Retorna todos os movimentos de produtos durante o intervalo de datas informado "start_date" e "end_date"
  

### Padrões de Projeto

- Padrão [Command](https://refactoring.guru/design-patterns/command) utilizado para centralizar a lógica de negócio e facilitar a manutenção do código
- Padrão [Repository](https://refactoring.guru/design-patterns/repository) utilizado para centralizar a lógica de acesso a dados e montagem de filtros utilizando o ORM Eloquent
