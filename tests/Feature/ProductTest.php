<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_if_can_create_product()
    {
        $productParams = Product::factory()->create()->toArray();

        $response = $this->post('/api/products', $productParams);

        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'quantity',
                    'created_at',
                    'updated_at',
                ],
            ]);

        $this->assertDatabaseHas('products', $productParams);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_if_can_return_a_product()
    {
        $product = Product::factory()->create();

        $response = $this->get('/api/products/'.$product->id);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'quantity',
                    'created_at',
                    'updated_at',
                ],
            ]);

        $this->assertDatabaseHas('products', $product->toArray());
    }
}
