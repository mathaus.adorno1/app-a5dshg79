<?php

namespace Tests\Feature;

use App\Models\Product;
use Tests\TestCase;

class ProductMovementTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_if_can_return_product_movement_array()
    {
        $response = $this->get('/api/product-movement');

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'product_id',
                        'quantity',
                        'created_at',
                        'updated_at',
                    ],
                ],
            ]);
    }

    /**
     * @test
     */
    public function test_if_can_create_product_movement()
    {
        $product = Product::factory()->create();

        $productMovementParams = [
            'sku' => $product->sku,
            'quantity' => 1,
            'type' => 'in',
        ];

        $response = $this->post('/api/product-movement', $productMovementParams);

        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'product_id',
                    'quantity',
                    'created_at',
                    'updated_at',
                ],
            ]);
    }
}
